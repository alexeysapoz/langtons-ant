import ui.LangstonsAntUI

object LangtonsAnt extends App {
  val ui = new LangstonsAntUI()
  ui.setVisible(true)
  ui.fitSizeToContent()
}
