package ui

import java.awt._
import javax.swing._
import ant.Board
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class BoardCanvas(
   board: Board,
   numberOfStepsPerFrame: Int = 1,
   periodBetweenFrames: Int = 10,
   boardCellSize: Int = 4
) extends JPanel {

  val antColor = Color.RED

  def boardWidth = board.width * boardCellSize
  def boardHeight = board.height * boardCellSize

  override protected def paintComponent(graphics: Graphics): Unit = {
    drawBoard(graphics)
    drawAnt(graphics)
  }

  private def drawBoard(graphics: Graphics): Unit = {
    setSize(boardWidth, boardHeight)
    setPreferredSize(new Dimension(boardWidth, boardHeight))
    graphics.clearRect(0, 0, boardWidth, boardHeight)
    for {
      (row, y) <- board.getCurrentState.zipWithIndex
      (cellState, x) <- row.zipWithIndex
    } drawCell(x, y, new Color(cellState.color), graphics)
  }
  
  private def drawCell(x: Int, y: Int, pointColor: Color, graphics: Graphics): Unit = {
    graphics.setColor(pointColor)
    graphics.fillRect(
      x * boardCellSize, y * boardCellSize,
      boardCellSize, boardCellSize
    )
  }

  private def drawAnt(graphics: Graphics): Unit = {
    val (antX, antY) = board.antCoordinates()

    graphics.setColor(antColor)
    graphics.fillRect(
      antX * boardCellSize, antY * boardCellSize,
      boardCellSize, boardCellSize
    )
  }

  def letGo(): Unit = {
    Future {
      var inBoard = true
      while (inBoard) {
        for(i <- 0 to numberOfStepsPerFrame) {
          if(inBoard)
            inBoard = board.makeStep()
        }
        repaint()
        Thread.sleep(periodBetweenFrames)
      }
    }
  }
}