package ui

import java.awt.BorderLayout
import javax.swing.JFrame
import ant.Board

class LangstonsAntUI() extends JFrame("Langton's ant") {
  val container = getContentPane
  container.setLayout(new BorderLayout())

  var canvas = new BoardCanvas(
    new Board(300, 300, "RL"),
    numberOfStepsPerFrame = 1,
    boardCellSize = 2
  )

  val controlPanel = new ControlPanel(
    onStart = (turnsStr: String) => {
      container.remove(canvas)
      canvas = new BoardCanvas(
        new Board(300, 300, turnsStr),
        numberOfStepsPerFrame = 4,
        boardCellSize = 2
      )
      canvas.letGo()
      container.add(canvas, BorderLayout.CENTER)
      container.validate()
    }
  )
  container.add(controlPanel,  BorderLayout.NORTH)
  container.add(canvas, BorderLayout.CENTER)


  def fitSizeToContent(): Unit = {
    val insets = getInsets
    setSize(
      canvas.boardWidth + insets.left + insets.right,
      canvas.boardHeight + controlPanel.getPreferredSize.getHeight.toInt + insets.top + insets.bottom
    )
    setResizable(false)
  }

}
