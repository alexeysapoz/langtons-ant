package ui

import java.awt.event.{ActionListener, ActionEvent}
import javax.swing.event.{DocumentListener, DocumentEvent}

object SwingUtils {
  def clickListener(handler: (ActionEvent) => Unit) =
    new ActionListener {
      override def actionPerformed(e: ActionEvent): Unit =
        handler(e)
    }

  def textChangeListener(handler: (DocumentEvent) => Unit) =
    new DocumentListener {
      override def insertUpdate(e: DocumentEvent): Unit =
        handler(e)

      override def removeUpdate(e: DocumentEvent): Unit =
        handler(e)

      override def changedUpdate(e: DocumentEvent): Unit =
        handler(e)
    }
}
