package ui

import java.awt.GridLayout
import javax.swing.{JButton, JTextField, JLabel, JPanel}
import SwingUtils.clickListener
import SwingUtils.textChangeListener
import ant.Turn

class ControlPanel(onStart: (String) => Unit) extends JPanel {
  setLayout(new GridLayout(1, 3))

  add(new JLabel("Input sequence:"))

  val sequenceInput = new JTextField()
  sequenceInput.getDocument.addDocumentListener(textChangeListener { e =>
    startButton.setEnabled(Turn.isValid(sequenceInput.getText))
  })
  add(sequenceInput)

  val startButton = new JButton("start")
  startButton.setEnabled(false)
  startButton.addActionListener(clickListener { e =>
    onStart(sequenceInput.getText)
  })
  add(startButton)
}
