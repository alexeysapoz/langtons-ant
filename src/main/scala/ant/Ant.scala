package ant

case class Ant(x: Int, y: Int, direction: Direction.Value) {

  def move(turn: Turn.Value): Ant = {
    val (newX, newY) =
      Direction.turn(direction, turn) match {
        case Direction.Up => (x, y - 1)
        case Direction.Right => (x + 1, y)
        case Direction.Down => (x, y + 1)
        case Direction.Left => (x - 1, y)
        case _ => (x, y)
      }

    Ant(
      x = newX,
      y = newY,
      direction = Direction.turn(direction, turn)
    )
  }
}
