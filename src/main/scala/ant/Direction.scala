package ant

object Direction extends Enumeration {
  val Up = Value(0)
  val Right = Value(1)
  val Down = Value(2)
  val Left = Value(3)

  def turn(currentDirection: Direction.Value, turn: Turn.Value) = {
    turn match {
      case Turn.Left =>
        turnLeft(currentDirection)
      case Turn.Right =>
        turnRight(currentDirection)
    }
  }

  def turnLeft(currentDirection: Direction.Value) = {
    if(currentDirection.id == 0) {
      Direction(3)
    } else {
      Direction(currentDirection.id - 1)
    }
  }

  def turnRight(currentDirection: Direction.Value) = {
    if(currentDirection.id == 3) {
      Direction(0)
    } else {
      Direction(currentDirection.id + 1)
    }
  }
}
