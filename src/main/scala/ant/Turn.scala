package ant

object Turn extends Enumeration {
  val Left = Value(0, "L")
  val Right = Value(1, "R")

  private val valuesNames: Set[String] =
    values.map(_.toString)

  private def isSymbolAllowed(valueName: String): Boolean =
    valuesNames.contains(valueName)

  def turnsFromStr(turnsStr: String): Seq[Turn.Value] =
    turnsStr
      .toCharArray
      .map(_.toString)
      .filter(isSymbolAllowed)
      .map(withName)

  def isValid(turnStr: String): Boolean =
    turnsFromStr(turnStr).size > 1
}
