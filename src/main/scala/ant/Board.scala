package ant

import java.awt.Color

class Board(val width: Int, val height: Int, val turns: Seq[Turn.Value]) {
  assert(turns.size >= 2)

  private val FirstBoardColor: Int = 0
  private val LastBoardColor: Int = new Color(255, 255, 255, 0).getRGB
  private val cells: Array[Array[CellState]] =
    Array.fill[CellState](width, height)(CellState(FirstBoardColor))
  private val colorStep = LastBoardColor / (turns.size - 1)

  def getCurrentState =
    cells.clone()

  private var ant = Ant(width/2, height/2, Direction.Up)

  val colors = turns.zipWithIndex.map { case (turn, index) =>
    val lastElemIndex = turns.size - 1
    index match {
      case 0 => FirstBoardColor
      case `lastElemIndex` => LastBoardColor
      case _ => index * colorStep
    }

    if(index != turns.size - 1) {
      index * colorStep
    } else {
      LastBoardColor
    }
  }

  private val colorsWithTurns =
    colors.zip(turns)

  private def turnOnCell(x: Int, y: Int): Turn.Value = {
    assert(x < width && y < height)
    val cellState = cells(y)(x)
    colorsWithTurns
      .find { case(color, turn) =>
        color == cellState.color
      }
      .fold(Turn.Left)(_._2)
  }

  def antCoordinates(): (Int, Int) = {
    val currentAntState = ant.copy()
    (currentAntState.x, currentAntState.y)
  }

  def makeStep(): Boolean =
    this.synchronized {
      cells(ant.y)(ant.x) = cells(ant.y)(ant.x).nextColor(colors)
      val newAnt = ant.move(turnOnCell(ant.x, ant.y))
      if(newAnt.x < width && newAnt.y < height) {
        ant = newAnt
        true
      } else {
        false
      }
    }

  def this(width: Int, height: Int, turnsStr: String) =
    this(width, height, Turn.turnsFromStr(turnsStr))
}