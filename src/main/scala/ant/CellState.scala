package ant

case class CellState(color: Int) {
  def nextColor(allowedColors: Seq[Int]) = {
    allowedColors.zipWithIndex.find { case (allowedColor, i) =>
      color == allowedColor
    }.fold(copy(color = allowedColors(0))) { case (allowedColor, i) =>
      if(i != allowedColors.size - 1)
        copy(color = allowedColors(i + 1))
      else
        copy(color = allowedColors(0))
    }
  }
}
